package br.com.justoeu.camel.domain;

import lombok.Data;

@Data
public class Person {

    private String nome;
    private String sobrenome;
    private String email;

}
