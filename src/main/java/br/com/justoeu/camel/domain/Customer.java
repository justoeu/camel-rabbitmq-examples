package br.com.justoeu.camel.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Customer {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
}