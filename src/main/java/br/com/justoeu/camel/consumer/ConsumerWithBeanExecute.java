package br.com.justoeu.camel.consumer;

import br.com.justoeu.camel.builder.RabbitConsumerConfigBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

@Slf4j
public class ConsumerWithBeanExecute {

    public static void main(String args[]) throws Exception {

        final ConsumerWithBeanExecute recv = new ConsumerWithBeanExecute();

        CamelContext context = new DefaultCamelContext();

        String cfgRabbit = new RabbitConsumerConfigBuilder().url("tmb-dev-free-rabbitmq-01.ns2online.com.br").port(5672)
                                                            .exchangeName("search.parentskus.test.direct")
                                                            .exchangeType("direct")
                                                            .queueName("search.parentskus.test.queue")
                                                            .autoAck(false).autoDelete(false).durable(true)
                                                            .concurrentConsumers(1).threadPoolSize(5)
                                                            .requestedHeartbeat(10)
                                                            .connectionTimeout(20000).requestTimeout(20000)
                                                            .prefetchEnabled(true).prefetchCount(100)
                                                            .username("freedom")
                                                            .password("freedom")
                                                            .vhost("freedom")
                                                            .build();

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from(cfgRabbit)
                        .bean(recv, "doWork(${body}, ${headers})")
                        .onException(Exception.class).markRollbackOnly().end();
            }
        });
//        context.setTracing(true);
        context.start();
    }


    public void doWork(String message, String headers) throws InterruptedException {
        log.info("[Mensagem] : " + message + " [headers] : " + headers);
        Thread.sleep(500);
    }

}

//        String cfgRabbit = "rabbitmq:tmb-dev-free-rabbitmq-01.ns2online.com.br:5672/search.parentskus.direct?" +
//                "threadPoolSize=5&concurrentConsumers=1&exchangeType=direct&autoDelete=false&durable=true&" +
//                "queue=search.parentskus.queue&autoAck=false&username=freedom&password=freedom&vhost=freedom&requestTimeout=0";

//        String cfgRabbit = "rabbitmq:10.0.98.29:5672/search.parentskus.direct?" +
//                "threadPoolSize=5&concurrentConsumers=1&exchangeType=direct&autoDelete=false&durable=true&" +
//                "queue=search.parentskus.queue&autoAck=false";

//        String cfgRabbit = "rabbitmq:10.0.97.68:5672/search.parentskus.direct?" +
//                "threadPoolSize=5&concurrentConsumers=1&exchangeType=direct&autoDelete=false&durable=true&" +
//                "queue=search.parentskus.queue&autoAck=false&requestTimeout=30000";


//        String cfgRabbit = "rabbitmq:192.168.99.100:5672/ha.monitoring.processNotifyExchange?" +
//                            "threadPoolSize=100&concurrentConsumers=400&exchangeType=topic&autoDelete=false&durable=true&" +
//                            "queue=ha.monitoring.processNotifyQueue&autoAck=false";

//10.0.98.70 - Artur

//        String cfgRabbit = "rabbitmq:localhost:5672/?" +
//                "threadPoolSize=5&concurrentConsumers=100&exchangeType=direct&autoDelete=false&durable=true&" +
//                "autoAck=false&queue=&requestedHeartbeat=10&connectionTimeout=20000&requestTimeout=20000" +
//                "&guaranteedDeliveries=true&prefetchCount=100&prefetchEnabled=true"; //&requestedHeartbeat=2&prefetchCount=1000&prefetchSize=10000

