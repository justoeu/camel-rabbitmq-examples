package br.com.justoeu.camel.consumer;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Recv {

    public static void main(String args[]) throws Exception {

        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("rabbitmq:192.168.99.100:5672/ha.monitoring.processNotifyExchange?" +
                        "threadPoolSize=10&exchangeType=topic&autoDelete=false&durable=true&queue=ha.monitoring.processNotifyQueue").to("stream:out");
            }
        });
        context.start();
    }
}
