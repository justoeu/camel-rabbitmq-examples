package br.com.justoeu.camel.producer;

import br.com.justoeu.camel.builder.CustomerBuilder;
import br.com.justoeu.camel.domain.Customer;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Message;
import org.apache.camel.component.rabbitmq.RabbitMQConstants;
import org.apache.camel.component.rabbitmq.RabbitMQEndpoint;
import org.apache.camel.component.rabbitmq.RabbitMQProducer;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SendNativeRabbitClient {


    public static void main(String args[]) throws Exception {
        ExecutorService exec = Executors.newFixedThreadPool(10);

        try {
            // Abaixo serve apenas para simular o "Abacos" populando a fila de atualizacao de precos
            exec.submit(() -> {
                for (int i = 0; i < 10000; i++) {
                    try {
                        Gson gson = new Gson();

                        ConnectionFactory rabbitConnFactory = new ConnectionFactory();
                        rabbitConnFactory.setHost("127.0.0.1");
                        final Connection conn;
                        try {
                            conn = rabbitConnFactory.newConnection();

                            final Channel channel = conn.createChannel();

                            channel.exchangeDeclare("exchangeDeExemploCammel", "direct", true);
                            channel.queueDeclare("queueDeExemploCammel", true, false, false, null);
                            channel.queueBind("queueDeExemploCammel", "exchangeDeExemploCammel", "bindingDeExemplo");

                            CustomerBuilder customerBuilder = new CustomerBuilder();
                            Customer customer = customerBuilder.buildFake();

                            channel.basicPublish("exchangeDeExemploCammel", "bindingDeExemplo", MessageProperties.PERSISTENT_TEXT_PLAIN, gson.toJson(customer).getBytes());

                            conn.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } finally {
            exec.shutdown();
        }
    }

    /**
     * Utilizando as classes do Camel para enviar para a Fila
     */
    public void enviarMensagemParaFila(String msg) throws Exception {
        RabbitMQEndpoint rabbitMQEndpoint = new RabbitMQEndpoint();
        rabbitMQEndpoint.setCamelContext(new DefaultCamelContext());
        rabbitMQEndpoint.setHostname("127.0.0.1");
        rabbitMQEndpoint.setPortNumber(5672);
        rabbitMQEndpoint.setAutoAck(false);
        rabbitMQEndpoint.setAutoDelete(false);
        rabbitMQEndpoint.setExchangeName("extCustomer");
        rabbitMQEndpoint.setQueue("customerQueue");
        rabbitMQEndpoint.setRoutingKey("routingKeyDeExemplo");
        rabbitMQEndpoint.setExchangeType("direct");

        RabbitMQProducer producer = new RabbitMQProducer(rabbitMQEndpoint);
        producer.start();

        Message message = new DefaultMessage();
        message.setBody(msg);
        message.setHeader("TesteDeHeader1", "Conteudo do Header 1");
        message.setHeader("TesteDeHeader2", "Conteudo do Header 2");
        message.setHeader("enconding", "UTF-8"); // Apenas para exemplificar o enriquecimento da mensagem
        message.setHeader(RabbitMQConstants.ROUTING_KEY, "routingKeyDeExemplo"); // funciona como bindingkey....
        message.setMessageId("OlhaOIdDaMensagem");

        Exchange e = new DefaultExchange(rabbitMQEndpoint, ExchangePattern.InOnly);
        e.setIn(message);

        producer.process(e);
        producer.stop();

    }
}
