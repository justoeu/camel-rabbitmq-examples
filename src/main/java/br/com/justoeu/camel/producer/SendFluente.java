package br.com.justoeu.camel.producer;

import br.com.justoeu.camel.builder.CustomerBuilder;
import br.com.justoeu.camel.commons.JsonUtils;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import java.util.Random;

public class SendFluente {

    public static void main(String args[]) throws Exception {

        CustomerBuilder builder = new CustomerBuilder();
        CamelContext context = new DefaultCamelContext();

        String msg = "{\"products\":[{\"uuid\":\"37230C8D-DE62-4797-9BB0-9B0069441B39\",\"generatedDateInMillis\":636122957671348390,\"sku\":\"025-1396-002-25\",\"country\":\"PT_BR\",\"categories\":[{\"category\":\"CAT_CHICO\",\"prices\":[{\"salePriceInCents\":\"3001\",\"listPriceInCents\":\"3001\",\"currency\":\"BRL\"}]},{\"category\":\"CAT_NETSHOESNOVA\",\"prices\":[{\"salePriceInCents\":\"3001\",\"listPriceInCents\":\"3001\",\"currency\":\"BRL\"}]},{\"category\":\"CAT_BROWNSHOESNOVA\",\"prices\":[{\"salePriceInCents\":\"5000\",\"listPriceInCents\":\"5000\",\"currency\":\"BRL\"}]}]}]}";

        //http://hmg-nsi-rabbitmq-01.ns2online.com.br:15672/#/queues/nsi/ha.nsi.eventQueue
//
//        "rabbitmq:tmb-dev-free-rabbitmq-01.ns2online.com.br:5672/search.parentskus.test.direct?" +
//                "threadPoolSize=10&concurrentConsumers=400&exchangeType=direct&autoDelete=false&durable=true&" +
//                "queue=search.parentskus.queue&autoAck=false&username=freedom&password=freedom&vhost=freedom"

        //&messageProperties=PERSISTENT_TEXT_PLAIN

        //                        .to("rabbitmq:localhost:5672/search.parentskus.direct?" +
//        "threadPoolSize=1&exchangeType=direct&autoDelete=false&durable=true&queue=search.parentskus.queue&username=guest&password=guest&networkRecoveryInterval=2000&automaticRecoveryEnabled=true") //&autoAck=true&username=admin&password=secret


        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("timer://simple?period=1").setBody().simple(msg + " >>> ${in.headers.CamelTimerCounter}")
                        .setHeader("rabbitmq.DELIVERY_MODE",constant(2))
                        .setHeader("rabbitmq.CONTENT_TYPE", constant("application/json"))
                        .setHeader("rabbitmq.CONTENT_ENCODING", constant("UTF-8"))
                        .setHeader("rabbitmq.PRIORITY",constant(0))
                        .to("rabbitmq:tmb-dev-free-rabbitmq-01.ns2online.com.br:5672/search.parentskus.test.direct?" +
                                        "threadPoolSize=10&concurrentConsumers=400&exchangeType=direct&autoDelete=false&durable=true&" +
                                        "queue=search.parentskus.test.queue&autoAck=false&username=freedom&password=freedom&vhost=freedom&networkRecoveryInterval=2000&automaticRecoveryEnabled=true"); //&autoAck=true&username=admin&password=secret
            }
        });




        //                        .to("rabbitmq:localhost:5672/search.parentskus.direct?" +
//        "threadPoolSize=1&exchangeType=direct&autoDelete=false&durable=true&queue=search.parentskus.queue&username=guest&password=guest&networkRecoveryInterval=2000&automaticRecoveryEnabled=true") //&autoAck=true&username=admin&password=secret

        // 10.0.97.68 eder

        //10.0.98.70

        //http://10.0.97.68:15672/


        //"rabbitmq:192.168.99.100:5672/search.parentskus.direct?"threadPoolSize=1&exchangeType=direct&autoDelete=false&durable=true&queue=search.parentskus.queue"
        context.setTracing(true);
        context.start();

//        Thread.sleep(2000);
//        context.stop();
    }

}
