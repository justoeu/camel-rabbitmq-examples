package br.com.justoeu.camel.producer;

import br.com.justoeu.camel.commons.JsonUtils;
import br.com.justoeu.camel.domain.Customer;
import com.google.gson.Gson;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.rabbitmq.RabbitMQConsumer;
import org.apache.camel.component.rabbitmq.RabbitMQEndpoint;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RPCServer {

    public static void main(String args[]) throws Exception {
        ExecutorService exec = Executors.newFixedThreadPool(1);

        try {
            exec.submit(() -> {
                try {
                    new RPCServer().consumer1();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } finally {
            exec.shutdown();
        }

    }


    //rabbitmq:10.0.97.68:5672/search.parentskus.direct?" threadPoolSize=5&concurrentConsumers=1&exchangeType=direct&autoDelete=false&durable=true& queue=search.parentskus.queue&autoAck=false"
    public void consumer1() throws Exception {

        RabbitMQEndpoint rabbitMQEndpoint = new RabbitMQEndpoint();
        rabbitMQEndpoint.setHostname("10.0.98.70");
        rabbitMQEndpoint.setPortNumber(5672);
        rabbitMQEndpoint.setAutoAck(false);
        rabbitMQEndpoint.setAutoDelete(false);
        rabbitMQEndpoint.setConcurrentConsumers(1);
        rabbitMQEndpoint.setQueue("search.parentskus.queue");
        rabbitMQEndpoint.setExchangeName("search.parentskus.direct");
        rabbitMQEndpoint.setExchangeType("direct");
        rabbitMQEndpoint.setThreadPoolSize(5);


        Processor processor = exchange -> {
            String msg="";
            try {
                Message in = exchange.getIn();
                Map<String, Object> headers = in.getHeaders();

                byte[] bytes = (byte[]) in.getBody();
                msg = new String(bytes);

                System.out.println("msg = " + msg);

                Thread.sleep(10000);
            } catch (Exception e) {
                //exchange.setException(e);
                e.printStackTrace();
            }
            new SendNativeRabbitClient().enviarMensagemParaFila(JsonUtils.toJson(msg));
        };
        RabbitMQConsumer consumer = new RabbitMQConsumer(rabbitMQEndpoint, processor);
        consumer.start();
        consumer.stop();
    }

//    public void consumer2() throws Exception {
//        Gson gson = new Gson();
//
//        RabbitMQEndpoint rabbitMQEndpoint = new RabbitMQEndpoint();
//        rabbitMQEndpoint.setHostname("127.0.0.1");
//        rabbitMQEndpoint.setPortNumber(5672);
//        rabbitMQEndpoint.setAutoAck(false);
//        rabbitMQEndpoint.setAutoDelete(false);
//        rabbitMQEndpoint.setConcurrentConsumers(10);
//        rabbitMQEndpoint.setQueue("customerQueue");
//        rabbitMQEndpoint.setExchangeName("extCustomer");
//        rabbitMQEndpoint.setExchangeType("direct");
//        rabbitMQEndpoint.setThreadPoolSize(5);
//
//
//        Processor processor = exchange -> {
//            try {
//                Message in = exchange.getIn();
//                Map<String, Object> headers = in.getHeaders();
//
//                byte[] bytes = (byte[]) in.getBody();
//                String e = new String(bytes);
//
//
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
////                exchange.setException(e);
//            }
//
//        };
//        RabbitMQConsumer consumer = new RabbitMQConsumer(rabbitMQEndpoint, processor);
//        consumer.start();
////        consumer.stop();
//    }

}
