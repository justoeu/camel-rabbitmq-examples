package br.com.justoeu.camel.builder;

/**
 * Created by valmir.justo on 25/10/16.
 */
public class RabbitConsumerConfigBuilder {

    private String url;
    private int port;
    private String exchangeName;
    private int threadPoolSize;
    private int concurrentConsumers;
    private String exchangeType;
    private boolean autoDelete;
    private boolean durable;
    private boolean autoAck;
    private String queueName;
    private int requestedHeartbeat;
    private int connectionTimeout;
    private int requestTimeout;
    private int prefetchCount;
    private boolean prefetchEnabled;
    private String username;
    private String password;
    private String vhost;

    //networkRecoveryInterval=2000
    //automaticRecoveryEnabled=true

    public RabbitConsumerConfigBuilder url(String url){
        this.url = url;
        return this;
    }

    public RabbitConsumerConfigBuilder port(int port){
        this.port = port;
        return this;
    }

    public RabbitConsumerConfigBuilder exchangeName(String exchangeName){
        this.exchangeName = exchangeName;
        return this;
    }

    public RabbitConsumerConfigBuilder threadPoolSize(int threadPoolSize){
        this.threadPoolSize = threadPoolSize;
        return this;
    }

    public RabbitConsumerConfigBuilder concurrentConsumers(int concurrentConsumers){
        this.concurrentConsumers = concurrentConsumers;
        return this;
    }

    public RabbitConsumerConfigBuilder exchangeType(String exchangeType){
        this.exchangeType = exchangeType;
        return this;
    }

    public RabbitConsumerConfigBuilder autoDelete(boolean autoDelete){
        this.autoDelete = autoDelete;
        return this;
    }

    public RabbitConsumerConfigBuilder durable(boolean durable){
        this.durable = durable;
        return this;
    }

    public RabbitConsumerConfigBuilder autoAck(boolean autoAck){
        this.autoAck = autoAck;
        return this;
    }

    public RabbitConsumerConfigBuilder queueName(String queueName){
        this.queueName = queueName;
        return this;
    }

    public RabbitConsumerConfigBuilder requestedHeartbeat(int requestedHeartbeat){
        this.requestedHeartbeat = requestedHeartbeat;
        return this;
    }

    public RabbitConsumerConfigBuilder connectionTimeout(int connectionTimeout){
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public RabbitConsumerConfigBuilder requestTimeout(int requestTimeout){
        this.requestTimeout = requestTimeout;
        return this;
    }

    public RabbitConsumerConfigBuilder prefetchCount(int prefetchCount){
        this.prefetchCount = prefetchCount;
        return this;
    }

    public RabbitConsumerConfigBuilder prefetchEnabled(boolean prefetchEnabled){
        this.prefetchEnabled = prefetchEnabled;
        return this;
    }

    public RabbitConsumerConfigBuilder username(String username){
        this.username = username;
        return this;
    }

    public RabbitConsumerConfigBuilder password(String password){
        this.password = password;
        return this;
    }

    public RabbitConsumerConfigBuilder vhost(String vhost){
        this.vhost = vhost;
        return this;
    }


    public String build(){
        StringBuilder sUrl = new StringBuilder();
        sUrl.append("rabbitmq:").append(this.url).append(":").append(this.port).append("/").append(this.exchangeName).append("?")
            .append("threadPoolSize=").append(this.threadPoolSize).append("&concurrentConsumers=").append(this.concurrentConsumers)
            .append("&exchangeType=").append(this.exchangeType).append("&autoDelete=").append(this.autoDelete).append("&durable=").append(this.durable)
            .append("&autoAck=").append(this.autoAck).append("&queue=").append(this.queueName)
            .append("&prefetchCount=").append(this.prefetchCount).append("&prefetchEnabled=").append(this.prefetchEnabled)
            .append("&requestedHeartbeat=").append(this.requestedHeartbeat)
            .append("&connectionTimeout=").append(this.connectionTimeout).append("&requestTimeout=").append(this.requestTimeout)
            .append("&username=").append(this.username).append("&password=").append(this.password)
            .append("&vhost=").append(this.vhost);

        return sUrl.toString();
    }

}